@section('styles')
    @parent

    <style rel="stylesheet">
        .widget-stat .media > span {
            height: 55px;
            width: 55px;
        }

        .widget-stat[class*="bg-"] .media > span {
            background-color: unset !important;
        }
    </style>
@endsection


<div class="cards-slider owl-carousel mb-4">
    <div class="items">
        <div class="widget-stat card bg-danger">
            <div class="card-body p-0 pe-2">
                <div class="media">
                    <span class="fs-3">
                        <i class="fa fa-desktop"></i>
                    </span>
                    <div class="media-body text-white">
                        <h5 class="text-white fs-6 m-0">
                        Office Manager
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="items">
        <div class="widget-stat card bg-success">
            <div class="card-body p-0 pe-2">
                <div class="media">
                    <span class="fs-3">
                        <i class="fa fa-money-bill"></i>
                    </span>
                    <div class="media-body text-white">
                        <h5 class="text-white fs-6 m-0">
                        Finance Department
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="items">
        <div class="widget-stat card bg-info">
            <div class="card-body p-0 pe-2">
                <div class="media">
                    <span class="fs-3">
                        <i class="fa fa-toolbox"></i>
                    </span>
                    <div class="media-body text-white">
                        <h5 class="text-white fs-6 m-0">
                        Maintenance Department
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="items">
        <div class="widget-stat card bg-primary">
            <div class="card-body p-0 pe-2">
                <div class="media">
                    <span class="fs-3">
                        <i class="fa fa-cog"></i>
                    </span>
                    <div class="media-body text-white">
                        <h5 class="text-white fs-6 m-0">
                        Service Department
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="items">
        <div class="widget-stat card bg-danger">
            <div class="card-body p-0 pe-2">
                <div class="media">
                    <span class="fs-3">
                        <i class="fa fa-car"></i>
                    </span>
                    <div class="media-body text-white">
                        <h5 class="text-white fs-6 m-0">
                        Valet Department
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="items">
        <div class="widget-stat card bg-success">
            <div class="card-body p-0 pe-2">
                <div class="media">
                    <span class="fs-3">
                        <i class="fas fa-balance-scale-left"></i>
                    </span>
                    <div class="media-body text-white">
                        <h5 class="text-white fs-6 m-0">
                        Legal Department
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-3">
        <div class="widget-stat card" style="height: unset">
            <div class="card-body p-3 pt-2 pb-2">
                <div class="media ai-icon">
                    <div class="media-body">
                        <p class="text-muted fs-6 mb-2">Customers</p>
                        <h2 class="text-primary fs-1 mb-0">1000</h2>
                    </div>
                    <span class="text-success">
                        <i class="fa fa-user"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3">
        <div class="widget-stat card" style="height: unset">
            <div class="card-body p-3 pt-2 pb-2">
                <div class="media ai-icon">
                    <div class="media-body">
                        <p class="text-muted fs-6 mb-2">Properties</p>
                        <h2 class="text-primary fs-1 mb-0">50</h2>
                    </div>
                    <span class="text-success">
                        <i class="fa fa-building"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3">
        <div class="widget-stat card" style="height: unset">
            <div class="card-body p-3 pt-2 pb-2">
                <div class="media ai-icon">
                    <div class="media-body">
                        <p class="text-muted fs-6 mb-2">Contracts</p>
                        <h2 class="text-primary fs-1 mb-0">700</h2>
                    </div>
                    <span class="text-success">
                        <i class="fa fa-file-contract"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3">
        <div class="widget-stat card" style="height: unset">
            <div class="card-body p-3 pt-2 pb-2">
                <div class="media ai-icon">
                    <div class="media-body">
                        <p class="text-muted fs-6 mb-2">Enquires</p>
                        <h2 class="text-primary fs-1 mb-0">1200</h2>
                    </div>
                    <span class="text-success">
                        <i class="fas fa-question-circle"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3">
        <div class="widget-stat card" style="height: unset">
            <div class="card-body p-3 pt-2 pb-2">
                <div class="media ai-icon">
                    <div class="media-body">
                        <p class="text-muted fs-6 mb-2">Maintenance</p>
                        <h2 class="text-primary fs-1 mb-0">800</h2>
                    </div>
                    <span class="text-success">
                        <i class="fa fa-toolbox"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3">
        <div class="widget-stat card" style="height: unset">
            <div class="card-body p-3 pt-2 pb-2">
                <div class="media ai-icon">
                    <div class="media-body">
                        <p class="text-muted fs-6 mb-2">Service</p>
                        <h2 class="text-primary fs-1 mb-0">500</h2>
                    </div>
                    <span class="text-success">
                        <i class="fa fa-cog"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3">
        <div class="widget-stat card" style="height: unset">
            <div class="card-body p-3 pt-2 pb-2">
                <div class="media ai-icon">
                    <div class="media-body">
                        <p class="text-muted fs-6 mb-2">Feedbacks</p>
                        <h2 class="text-primary fs-1 mb-0">500</h2>
                    </div>
                    <span class="text-success">
                        <i class="fas fa-comment"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<h4 class="card-title text-primary">Latest Enquiries</h4>
<div class="card mt-4">
    <div class="card-body p-0 pb-3">  
        <table class="display align-middle" id="table">
            <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Enquiry Id</th>
                <th>Customer</th>
                <th>Mobile</th>
                <th>Property Id</th>
                <th>Property</th>
                <th>Date</th>
                <th class="text-center">Options</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">1</td>
                    <td>E-101</td>
                    <td>Taha Bubai</td>
                    <td>23456789</td>
                    <td>P-101</td>
                    <td>Atwad Tower</td>
                    <td>25th Dec 2021</td>
                    <td class="text-center">
                        <a class="btn btn-xs btn-outline-primary" href="#">Edit</a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td>E-102</td>
                    <td>Fahad Ali</td>
                    <td>24567891</td>
                    <td>P-102</td>
                    <td>Baitak Tower</td>
                    <td>02nd Jan 2022</td>
                    <td class="text-center">
                        <a class="btn btn-xs btn-outline-primary" href="#">Edit</a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">3</td>
                    <td>E-103</td>
                    <td>Khaled</td>
                    <td>25678910</td>
                    <td>P-103</td>
                    <td>Hamra Tower</td>
                    <td>04th Jan 2022</td>
                    <td class="text-center">
                        <a class="btn btn-xs btn-outline-primary" href="#">Edit</a>
                </tr>
            </tbody>
        </table>      
    </div>
</div>

@push('scripts')    
    <script type="text/javascript">
        $(document).ready(function() {
            jQuery('.cards-slider').owlCarousel({
                loop:false,
                margin:30,
                nav:true,
                rtl:(getUrlParams('dir') == 'rtl') ? true : false,
                autoWidth:true,
                dots: false,
                navText: ['', ''],
            });

            $('#table').DataTable({
                dom: '<>t<>',
                autoWidth: false,
                responsive: false,
                searching: false,
                searchHighlight: false,
                info: false,
                paging: false,
                lengthChange: false,
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
                ordering: false,
                order: [[0, 'DESC']],
                processing: true,
                serverSide: false,
                deferRender: true,
                stateSave: false,           
            });
        });  
    </script>
@endpush