<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Login</title>

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" sizes="16x16" href="{{ asset('favicon.ico') }}">

    @if (App::getLocale() == 'en')
        <!-- Required CSS -->
        <!-- bootstrap select -->
        <link rel="stylesheet" href="{{ asset('themes/Kripton/ltr/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
        <!-- style -->
        <link rel="stylesheet" href="{{ asset('themes/Kripton/ltr/css/style.css') }}">
    @endif

    @if (App::getLocale() == 'ar')
        <!-- Required CSS -->
        <!-- bootstrap select -->
        <link rel="stylesheet" href="{{ asset('themes/Kripton/rtl/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
        <!-- style -->
        <link rel="stylesheet" href="{{ asset('themes/Kripton/rtl/css/style.css') }}">
    @endif
</head>

<body class="vh-100">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center d-flex flex-column">
                <div class="d-flex justify-content-center mb-3">
                    <a href="{{ route('login') }}"><span class="fs-2 text-primary"></span></a>
                </div>
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    
                                    <h4 class="text-center text-primary mb-4">Sign in your account</h4>
                                    <form method="POST" action="{{ route('login') }}" autocomplete="off">
                                    @csrf

                                        <div class="form-group">
                                            <label class="mb-1"><strong>Email</strong></label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" value="superadmin@domain.com" name="email" id="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror    
                                        </div>

                                        <div class="form-group">
                                            <label class="mb-1"><strong>Password</strong></label>
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" value="12345678" id="password" name="password">
                                            
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror    
                                        </div>

                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                               <div class="form-check custom-checkbox ms-1">
                                                    <input type="checkbox" class="form-check-input" id="remember" name="remember" 
                                                    {{ old('remember') ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="remember">Keep me signed in</label>
                                                </div>
                                            </div>
                                            <div class="d-none form-group">
                                                <a href="{{ route('password.request') }}">Forgot Password?</a>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                                        </div>
                                    </form>
                                    <div class="d-none new-account mt-3">
                                        <p>Don't have an account? <a class="text-primary" href="{{ route('register') }}">Sign up</a></p>
                                    </div>

                                 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (App::getLocale() == 'en')
        <!-- Required vendors -->
        <!-- global -->
        <script src="{{ asset('themes/Kripton/ltr/vendor/global/global.min.js') }}"></script>
        <!-- bootstrap-select Js -->
        <script src="{{ asset('themes/Kripton/ltr/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
        <!-- custom Js -->
        <script src="{{ asset('themes/Kripton/ltr/js/custom.min.js') }}"></script>
        <!-- deznav-init -->
        <script src="{{ asset('themes/Kripton/ltr/js/deznav-init.js') }}"></script>
    @endif

    @if (App::getLocale() == 'ar')
        <!-- Required vendors -->
        <!-- global -->
        <script src="{{ asset('themes/Kripton/rtl/vendor/global/global.min.js') }}"></script>
        <!-- bootstrap-select Js -->
        <script src="{{ asset('themes/Kripton/rtl/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
        <!-- custom Js -->
        <script src="{{ asset('themes/Kripton/rtl/js/custom.min.js') }}"></script>
        <!-- deznav-init -->
        <script src="{{ asset('themes/Kripton/rtl/js/deznav-init.js') }}"></script>
    @endif
</body>
</html>
